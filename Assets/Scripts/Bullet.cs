using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float bulletSpeed;
    public bool shootsUp;

    void Update()
    {
        transform.position += new Vector3(0, Time.deltaTime * (shootsUp ? bulletSpeed : -bulletSpeed));
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Fragate" || collision.gameObject.tag == "SpeedFlight")
            collision.gameObject.GetComponent<Enemy>().MinusHp();
    }
}
