using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject fragate;
    public GameObject speedFlight;

    public float minSpawnRange;
    public float maxSpawnRange;

    public float timeBetweenSpawns;
    private float _timer;

    private void Start()
    {
        _timer = 0;
    }

    private void Update()
    {
        _timer += Time.deltaTime;
        if (_timer > timeBetweenSpawns)
        {
            _timer = 0;
            GenerateSpaceship();
        }
    }

    private void GenerateSpaceship()
    {
        GameObject[] array = new GameObject[] { fragate, speedFlight };
        Instantiate(array[Random.Range(0, 2)]).transform.position = new Vector3(transform.position.x + Random.Range(minSpawnRange, maxSpawnRange), transform.position.y);
    }
}
