using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int healthPoints;

    private void Update()
    {
        if (healthPoints <= 0)
            Destroy(gameObject);
    }

    public void MinusHp()
    {
        healthPoints -= 1;
    }
}
