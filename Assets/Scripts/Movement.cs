using UnityEngine;

public class Movement : MonoBehaviour
{
    public bool autonomousMovement;

    public float speedX;
    public float speedY;

    private float blockX;
    private float blockY;

    private void Start()
    {
        blockX = 0;
        blockY = 0;
    }

    void Update()
    {
        if (!autonomousMovement)
        {
            blockX = transform.position.x;
            blockY = transform.position.y;

            transform.position += new Vector3(
                 Time.deltaTime * speedX * ((blockX < 12.5f && blockX > -12.5f) ? 0 : 1),
                 Time.deltaTime * speedY * ((blockY < 9.3f && blockY > -2.5f) ? 0 : 1));
        }            

        transform.position += new Vector3(
            Time.deltaTime * speedX * (autonomousMovement ? 0 : Input.GetAxis("Horizontal")), 
            Time.deltaTime * speedY * (autonomousMovement ? -1 : Input.GetAxis("Vertical")));

        
    }
}
