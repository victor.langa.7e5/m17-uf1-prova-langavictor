using UnityEngine;

public class Shoot : MonoBehaviour
{
    public GameObject bullet;
    public bool isPlayer;
    public float shootCooldown;
    private float _timer;

    private Collider2D collider;

    private void Start()
    {
        collider = GetComponent<Collider2D>();
        _timer = 0;
    }

    void Update()
    {
        switch (isPlayer)
        {
            case true:
                if (Input.GetKeyDown(KeyCode.Space))
                    Instantiate(bullet).transform.position = new Vector2(collider.bounds.center.x, isPlayer ? collider.bounds.max.y : collider.bounds.min.y);
                break;
            case false:
                _timer += Time.deltaTime;
                if (_timer > shootCooldown)
                {
                    Instantiate(bullet).transform.position = new Vector2(collider.bounds.center.x, isPlayer ? collider.bounds.max.y : collider.bounds.min.y);
                    _timer = 0;
                }
                break;
        }
    }
}
