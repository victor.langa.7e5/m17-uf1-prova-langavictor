using UnityEngine;

public class Destroy : MonoBehaviour
{
    bool scoreCounts;

    private void Start()
    {
        scoreCounts = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Death")
        { 
            scoreCounts = false;
            Destroy(gameObject);
        }
        if ((gameObject.tag == "Fragate" || gameObject.tag == "SpeedFlight") && collision.gameObject.tag != "EnemyBullet" && collision.gameObject.tag != "Player")
            Destroy(gameObject);
        if (gameObject.tag == "Player")
            Destroy(gameObject);
    }

    private void OnDestroy()
    {
        float score = 0;

        if (scoreCounts)
            switch (gameObject.tag)
            {
                case "Fragate":
                    score = 20;
                    break;
                case "SpeedFlight":
                    score = 5;
                    break;
            }

        GameManager.Instance.score += score;
    }
}
