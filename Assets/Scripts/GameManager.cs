using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance { get => _instance; }

    public TextMeshProUGUI scoreText;
    public float score;

    private void Awake()
    {
        if (_instance != null && _instance != this) Destroy(this);
        _instance = this;
    }

    private void Start()
    {
        score = 0;
    }

    private void Update()
    {
        scoreText.text = score.ToString();
    }
}
